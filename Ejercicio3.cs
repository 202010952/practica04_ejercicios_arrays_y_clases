using System;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
               //3.	Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto), pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.
                int[] meses = {31,28,31,30,31,30,31,31,30,31,30,31};
                int mes = 0;
                Console.WriteLine("Favor indicar el numero del mes que desea validar: Ejemplo (1=enero, 12=diciembre) \n");
                mes = int.Parse(Console.ReadLine());
                switch (mes)
                {
                    case 1:
                        mes = 1;
                        Console.Write("El mes seleccionado fue Enero, que tiene: "+meses[0]+" dias\n");
                        break;
                    case 2:
                         mes = 2;
                         Console.Write("El mes seleccionado fue Febrero, que tiene: " + meses[1] +" dias\n");
                         break;
                    case 3:
                         mes = 3;
                         Console.Write("El mes seleccionado fue Marzo, que tiene: " + meses[2] +" dias\n");
                         break;
                    case 4:
                         mes = 4;
                         Console.Write("El mes seleccionado fue Abril, que tiene: " + meses[3] +" dias\n");
                         break;
                    case 5:
                         mes = 5;
                         Console.Write("El mes seleccionado fue Mayo, que tiene: " + meses[4] +" dias\n");
                         break;
                    case 6:
                         mes = 6;
                         Console.Write("El mes seleccionado fue Junio, que tiene: " + meses[5] +" dias\n");
                         break;
                    case 7:
                         mes = 7;
                         Console.Write("El mes seleccionado fue Julio, que tiene: " + meses[6] +" dias\n");
                         break;
                    case 8:
                         mes = 8;
                         Console.Write("El mes seleccionado fue Agosto, que tiene: " + meses[7] +" dias\n");
                         break;
                    case 9:
                         mes = 9;
                         Console.Write("El mes seleccionado fue Septiembre, que tiene: " + meses[8] +" dias\n");
                         break;
                    case 10:
                         mes = 10;
                         Console.Write("El mes seleccionado fue Octubre, que tiene: " + meses[9] +" dias\n");
                         break;
                    case 11:
                         mes = 11;
                         Console.Write("El mes seleccionado fue Noviembre, que tiene: " + meses[10] +" dias\n");
                         break;
                    case 12:
                         mes = 12;
                         Console.Write("El mes seleccionado fue Diciembre, que tiene: " + meses[11] +" dias\n");
                         break;
                    default:
                         Console.Write("El valor ingresado no es valido");
                         break;
                }
          }
    }
}
