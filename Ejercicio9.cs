using System;

namespace Ejercicio9
{
     class TablaMultiplicar
     {
          public void CargarValor()
          {
               int valor;
               string lector;
               do
               {
                    Console.Write("Ingrese un valor (-1 para finalizar): ");
                    lector = Console.ReadLine();
                    valor = int.Parse(lector);
                    if (valor != -1)
                    {
                         Calcular(valor);
                    }
               } while (valor != -1);
          }

          public void Calcular(int v)
          {
               for (int x = v; x <= v * 12; x = x + v)
               {
                    Console.Write(x + "-");
               }
               Console.WriteLine();
          }

          static void Main(string[] args)
          {
               TablaMultiplicar tm = new TablaMultiplicar();
               tm.CargarValor();
          }
     }
}
