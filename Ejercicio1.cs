using System;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
               //1.	Un programa que pida al usuario 4 números, los memorice (utilizando un array), calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.
               double mediaAritmetica;
               double[] miArreglo = new double[4];
               for(int i=0; i<4;i++){
                   Console.WriteLine("Favor introducir los valores a calcular\n");
                   miArreglo[i] = int.Parse(Console.ReadLine());
               }
                  Console.Write("Los valores introducidos por el usuario fueron: \n" + " " + miArreglo[0] + ", " + miArreglo[1] + ", " + miArreglo[2] + " y " + miArreglo[3]+"\n");
                  mediaAritmetica = (miArreglo[0]+miArreglo[1]+miArreglo[2]+miArreglo[3])/4;
                  Console.Write("El valor de la media aritmetica de los valores introducidos por teclado es igual a: "+mediaAritmetica+"\n\n");
          }
    }
}
