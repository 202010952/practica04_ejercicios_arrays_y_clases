using System;

namespace PruebaVector2
{
     class Arreglojercicio8
     {
          private float[] estatura;
          private float promedio;

          public void Cargar()
          {
               estatura = new float[5];
               for (int x = 0; x < 5; x++)
               {
                    Console.Write("Ingrese la estatura de las personas: \n");
                    string lector = Console.ReadLine();
                    estatura[x] = float.Parse(lector);
               }
          }

          public void CalcularPromedio()
          {
               float suma;
               suma = 0;
               for (int x = 0; x < 5; x++)
               {
                    suma = suma + estatura[x];
               }
               promedio = suma / 5;
               Console.WriteLine("Promedio de Estatura es igual a: " + promedio);
          }

          public void MayoresMenores()
          {
               int mayor=0, menor=0;
               for (int x = 0; x < 5; x++)
               {
                    if (estatura[x] > promedio)
                    {
                         mayor++;
                    }
                    else
                    {
                         if (estatura[x] < promedio)
                         {
                              menor++;
                         }
                    }
               }
               Console.WriteLine("Cantidad de personas mayores al promedio: " + mayor);
               Console.WriteLine("Cantidad de personas menores al promedio: " + menor);
               Console.ReadKey();
          }

          static void Main(string[] args)
          {
               Arreglojercicio8 ae8 = new Arreglojercicio8();
               ae8.Cargar();
               ae8.CalcularPromedio();
               ae8.MayoresMenores();
          }
     }
}