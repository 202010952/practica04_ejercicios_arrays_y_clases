using System;

namespace Ejercicio4
{
    class Arreglo10Posiciones
    {
        private int[] num;
        private int Nmayor=0;
        private int suma;
        public void CargarArreglo()
        {
            num = new int[10];
            for (int x = 0; x<10;x++)
            {
                Console.Write("Ingrese un valor: \n");
                string lector;
                lector = Console.ReadLine();
                num[x] = int.Parse(lector);
            }
        }
        public void Imprimir()
        {  //Identificar el mayor numero ingresado
           int mayor = 0;
           for (int i = 0; i <10; i++)
           {
              if(num[i] > Nmayor)
              {
                  mayor = num[i];
              } 
           }Console.WriteLine("El numero mayor ingresado por teclado fue: "+mayor+"\n");
           // Imprimir el listado de numeros ingresados por teclado
           for (int x = 0; x <10; x++)
           {
                Console.WriteLine(num[x]);
           }
           suma = num[0]+num[1]+num[2]+num[3]+num[4]+num[5]+num[6]+num[7]+num[8]+num[9];
           Console.WriteLine("El valor total de los datos ingresados es igual a: "+suma);
           Console.ReadKey(); 
           
        }

        static void Main(string[] args)
        {
               //4.	Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.
              Arreglo10Posiciones a1p = new Arreglo10Posiciones();
              a1p.CargarArreglo();
              a1p.Imprimir();
        }
    }
}

