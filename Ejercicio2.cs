using System;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
               //2.	Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.
               float[] numf = new float[5];
               for (int i = 0; i < 5; i++)
               {
                    Console.WriteLine("Favor introducir los valores.\n");
                    numf[i] = int.Parse(Console.ReadLine());
               }
               Console.Write("Los valores intruducidos fueron: " + numf[4] + "," + numf[3] + "," + numf[2] + "," + numf[1] + "," + numf[0] + "\n\n");
        }
    }
}
