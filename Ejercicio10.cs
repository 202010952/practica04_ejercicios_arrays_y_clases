using System;

namespace Ejercicio10
{
class Persona
     {
          private string nombre;
          private int edad;

          public void DatosPersona()
          {
               Console.Write("Ingrese el nombre: \n");
               nombre = Console.ReadLine();
               string linea;
               Console.Write("Ingrese la edad: \n");
               linea = Console.ReadLine();
               edad = int.Parse(linea);
          }

          public void Imprimir()
          {
               Console.Write("Nombre: ");
               Console.WriteLine(nombre);
               Console.Write("Edad: ");
               Console.WriteLine(edad);
          }

          public void ValidarEdad()
          {
               if (edad >= 18)
               {
                    Console.Write("Es mayor de edad");
               }
               else
               {
                    Console.Write("No es mayor de edad");
               }
               Console.ReadKey();
          }


          static void Main(string[] args)
          {
               Persona per = new Persona();
               per.DatosPersona();
               per.Imprimir();
               per.ValidarEdad();
          }
     }
}
